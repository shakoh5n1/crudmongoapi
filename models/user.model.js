const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;



let UserSchema = new Schema({
    sex: String,
    name: String,
    surname: String,
    dob: Date,
    email: String,
    password: String,
    country: String
},
{
    collection: 'users'
}
);

UserSchema.pre('save', function(next){
    let user = this;
if (!user.isModified('password')) return next();

Bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt){
    if(err) return next(err);

    Bcrypt.hash(user.password, salt, function(err, hash){
        if(err) return next(err);

        user.password = hash;
        next();
    });
});
});

module.exports = mongoose.model('user', UserSchema);

