const express = require('express');
const bodyParser = require('body-parser');
// const multer = require('multer');
// const upload = multer({dest: __dirname + '/uploads/images'});
// var path = require('path');
const product = require('./routes/product.route');
const user = require('./routes/user.route');
const zoomer = require('./routes/zoomer.route');
const app = express();

const authRouts = require('./routes/authRouter');
var cookieParser = require('cookie-parser');
const expressValidator = require('express-validator');
const cors = require('cors')

const mongoose = require('mongoose');
// let dev_db_url = ('mongoDB://127.0.0.1:27017/test', {useNewUrlParser: true});
// let mongoDB = process.env.MONGODB_URI || dev_db_url;

// mongoose.connect(mongoDB);

// mongoose.connect('mongodb://134.209.29.20/test', {useNewUrlParser: true});

mongoose.connect('mongodb://134.209.29.20/zoomerDB', {useNewUrlParser: true});

mongoose.Promise = global.Promise;

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));
app.use(cookieParser());

app.use(bodyParser.json());
app.use(expressValidator());
app.use(cors())

app.use('/products', product);
app.use('/user', user);
app.use('/zoomer', zoomer);
app.use('/', zoomer );
app.use('/uploads/images' , express.static('uploads/images') );

app.use('/', authRouts)


app.listen(5000, () => {
    console.log('listens')
});





