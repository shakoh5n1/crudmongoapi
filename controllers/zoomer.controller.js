const Product = require('../models/zoomer.model');

// const multer = require('multer');

// const storage = multer.diskStorage({
//     destination: function(req, file, cb) {
//         cb(null, './uploads/images/');
//     },
//     filename: function(req, file,cb) {
//         cb(null, file.originalname)
//     }
// })

// const upload = multer({storage: storage});

exports.addProduct = function (req, res, next) {
    let path = req.file.path
    let path2 = path.replace(/\\/g, "/")
    let productList = new Product(
        {
            title: req.body.title,
            img: path2,
            price: req.body.price,
            company: req.body.company,
            info: req.body.info,
            inCart: req.body.inCart,
            count: req.body.count,
            type: req.body.type,
            total: req.body.total
        }
    );

    productList.save(function(err) {
        if(err) {
            return next(err);
        }
        res.send(
            // productList
            {
            message : "Product add",
            status : 'ok'
        }
        );
    });


};


exports.getProductList = function (req, res) {
    Product.find(req.find, function (err, productList) {
        if (err) return next(err);
        res.send(productList)
    })
}

exports.getDetailProduct = function (req, res) {
    Product.findById({ '_id' : req.params.productId }, function (err, Product) {
        if (err) res.send(err)
        res.send({
            Product
            // message : 'Updated',
            // status: 'ok'
        });
        
    });
}
exports.getProductByType = function (req, res) {
    Product.find({ type: 'mobile' }, function (err, Product) {
        if (err) res.send(err)
        res.send({
            Product
            // message : 'Updated',
            // status: 'ok'
        });
        
    });
}
