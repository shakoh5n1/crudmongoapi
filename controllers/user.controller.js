const User = require('../models/user.model');
const Bcrypt = require('bcrypt');


exports.userRegister = function (req, res) {
    let user = new User(
        {
            sex: req.body.sex,
            name: req.body.name,
            surname: req.body.surname,
            dob: req.body.dob,
            email: req.body.email,
            password: req.body.password,
            country: req.body.country
        }
    );

    user.save(function(err) {
        if(err) {
            return next(err);
        }
        res.send({
            message : 'User registered successfully',
            status: 'ok'
        });
    });
};


exports.userList = function (req, res) {
    User.find(req.find, function (err, user) {
        if (err) return next(err);
        res.send(user)
    })
}

exports.user_delete = function (req, res) {
    User.deleteOne(req.params.id, function(err) {
        if(err) return next(err);
        res.send({
            message : 'Deleted',
            status: 'ok'
        });
    })
};


exports.user_update = function (req, res) {
    console.log(req.params);
    User.updateOne({ '_id' : req.params.userId }, {name: req.body.name}, function (err, user) {
        if (err) res.send(err)
        res.send({
            message : 'Updated',
            status: 'ok'
        });
    });
};

exports.userLogin = function (req, res) {
   
    User.findOne({ 'email' : req.body.email },function (err, user){
    //   console.log(user.email)
    //   if(err) {
    //       res.send({
    //           status: 403,
    //           message: 'nouser'
    //       })
    //   }
    })

    .then(function(user) {
        
        return Bcrypt.compare(req.body.password, user.password);

    })

    .then(function(samePassword) {

        if(!samePassword) {
            res.send({
                status: 403,
                message : 'nouser'
            });
        } else {
            res.send({
                status: 200,
                message: 'ok'
            });
        }
        
    })

    .catch(function(error){
        console.log("Error authenticating user: ");
        console.log(error);
        next();
    });
}

    

