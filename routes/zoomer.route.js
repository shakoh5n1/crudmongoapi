const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

const multer = require('multer');
// const upload = multer({dest: __dirname + '/uploads/images'});
// var path = require('path');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/images/' );
    },
    filename: function(req, file, cb) {
        
        cb(null, file.originalname )
        
    }
})

const upload = multer({storage: storage});


router.use(bodyParser.json())

const zoomerController = require('../controllers/zoomer.controller');

router.post('/addproduct', upload.single('img'), zoomerController.addProduct, (req, res) => {

   res.send(req.file.originalname)
} );

router.get('/productlist', zoomerController.getProductList);

router.get('/filter', zoomerController.getProductByType)

router.get('/details/:productId' , zoomerController.getDetailProduct)

// router.post('/upload', upload.single('selectedFile'), (req, res) => {

//     console.log(req.file) // form files
//    res.send(req.file.originalname)
// });

// router.get('/',)
module.exports = router;

