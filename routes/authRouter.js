const router = require('express').Router()
const { signup, signin, signout } = require('../controllers/authController.js')
const { userSignupValidator } = require('../validator/index.js')
// const {userById } = require('../controllers/userController.js')



router.post('/signup',userSignupValidator, signup)
router.post('/signin', signin)
router.get('/signout', signout)

// router.param("userId", userById)

module.exports = router
