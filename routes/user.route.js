const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

router.use(bodyParser.json());

const userController = require('../controllers/user.controller');

router.get('/userList', userController.userList);

router.post('/registration', userController.userRegister);

router.post('/login', userController.userLogin);

router.delete('/delete/:userId' , userController.user_delete);

router.put('/update/:userId' , userController.user_update)

module.exports = router;

