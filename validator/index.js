exports.createPostValidator = (req,res,next)=>{
	//check title
	req.check("title", "Write a title").notEmpty();
	req.check("title", "title mus be between 4 to 150 characters").isLength({
		min: 4,
		max: 150
	});
	//check body
	req.check("body", "body a title").notEmpty();
	req.check("body", "body mus be between 4 to 150 characters").isLength({
		min: 4,
		max: 200
	});
	//check for errors
	const errors = req.validationErrors();
	//if error show the first one
	if(errors){
		const firstError = errors.map(error => error.msg)[0]
		return res.status(400).json({error: firstError})
	}
	//proceed to next midlware
	next()	
}

exports.userSignupValidator = (req, res, next)=>{
	//name is not null and its between 4 -10
	req.check("name", "Name might not be empty").notEmpty()
	//check for email
	req.check("email", "Email must be between 3 - 30 characters")
	.matches(/.+\@.+\..+/)
	.withMessage("Email must contain @")
	.isLength({
		min: 4,
		max: 2000
	})
	//check for password
	req.check("password", "password is required").notEmpty();
	req.check("password")
	.isLength({min: 6})
	.withMessage("password must contain at least 6 characters")
	.matches(/\d/)
	.withMessage("password must conatin number")

	//check for errors
	const errors = req.validationErrors();
	if(errors){
		const firstError = errors.map(error => error.msg)[0]
		return res.status(400).json({error: firstError})
	}
	next()
}